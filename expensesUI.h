#ifndef expensesUI_h
#define expensesUI_h

#include "conf.h"

void collapseExpensesWindowsOnFirstDraw(struct nk_context *ctx, struct UIState *ui_state);

void errorLabels(struct nk_context *ctx, struct Item *item, char *action);

void viewExpensesWindow(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers); 
void nameFilterWidgetsInExpenses(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers);
void dateFilterWidgetsInExpenses(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers);
void sortExpenses(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers);

void inputExpensesWindow(struct nk_context *ctx, struct Item *item, struct FileTags *file_tags, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers);
void chooseDate(struct nk_context *ctx, char *buffer);
void saveExpense(struct nk_context *ctx, struct Item *item, struct FileTags *file_tags, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers);

void getData(struct nk_context *ctx, void (*clearFunction) (struct Item *item), struct Item *item);

void updateExpensesWindow(struct nk_context *ctx, struct Item *item, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers);
void updateExpense(struct nk_context *ctx, struct Item *item, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers);

void deleteExpensesWindow(struct nk_context *ctx, struct Item *item, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers);
void deleteAllExpenses(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers);
void deleteExpense(struct nk_context *ctx, struct Item *item, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers);

void totalsWindow(struct nk_context *ctx, struct TotalBuffers *total_buffers);
void dateFilterWidgetsInTotals(struct nk_context *ctx, struct TotalBuffers *total_buffers);

void showAbout(struct nk_context *ctx);
void changeTheme(struct nk_context *ctx, struct Conf *conf);
void settingsWindow(struct nk_context *ctx, struct UIState *ui_state, struct Conf *conf);
void proceedToTags(struct UIState *ui_state);
void proceedToChangePassword(struct UIState *ui_state);

#endif
