#ifndef dates_h
#define dates_h

#include <stdbool.h>

void padDate(char *date);
void formatDateToISO(char *date);
const char* unformatDateFromISO(const char *date);
bool isDateFormatValid(char *date);
bool isDateValid(char *date);

#endif
