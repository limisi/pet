#include <string.h>
#include "itemSizes.h"//NAME_SIZE
#include "dates.h"
#include "filterUtils.h"

#define FILTER_DATE_OK 1
#define FILTER_DATE_WRONG 0

int checkFilterDate(char *filter_date){
  if (strlen(filter_date) == 0) return FILTER_DATE_WRONG;  
  padDate(filter_date);
  if (!isDateFormatValid(filter_date)) return FILTER_DATE_WRONG;
  if (!isDateValid(filter_date)) return FILTER_DATE_WRONG;
  return FILTER_DATE_OK;
}

void addPercentSigns(char *name){
  int name_length = strlen(name);
  char temp[NAME_SIZE + 5];
  strcpy(temp, name);
  name[0] = '%';
  strcpy(name + 1, temp);
  name[name_length + 1] = '%';
  name[name_length + 2] = '\0'; 
}

void addSingleQuotes(char *string){
  int string_length = strlen(string);
  char temp[NAME_SIZE + 5];
  strcpy(temp, string);
  string[0] = '\'';
  strcpy(string + 1, temp);
  string[string_length + 1] = '\'';
  string[string_length + 2] = '\0';
}

void applyNameFilterToExpenses(struct ExpenseBuffers *expense_buffers, char *filter_name){
  static char filter_name_formatted[NAME_SIZE + 5];
  if (strlen(filter_name) != 0){
    strcpy(filter_name_formatted, filter_name);
    addPercentSigns(filter_name_formatted);
    addSingleQuotes(filter_name_formatted);
    expense_buffers->filter_name = filter_name_formatted;
    freeExpenseBuffers(expense_buffers);
    loadExpenses(expense_buffers);
  } 
}

void clearNameFilterFromExpenses(struct ExpenseBuffers *expense_buffers, char *filter_name){
  int isFiltered = strcmp(expense_buffers->filter_name, "'%'");
  if (isFiltered){
    expense_buffers->filter_name = "'%'";
    freeExpenseBuffers(expense_buffers);
    loadExpenses(expense_buffers);
    memset(filter_name,'\0', strlen(filter_name));
  }
}

void applyDateFilterToExpenses(struct ExpenseBuffers *expense_buffers, char *start_date, char *end_date){
  static char start_date_formatted[DATE_SIZE + 2], end_date_formatted[DATE_SIZE + 2];
  strcpy(start_date_formatted, start_date);
  strcpy(end_date_formatted, end_date);
  if (checkFilterDate(start_date_formatted) && checkFilterDate(end_date_formatted)){
    formatDateToISO(start_date_formatted);
    addSingleQuotes(start_date_formatted);
    expense_buffers->start_date = start_date_formatted;
    
    formatDateToISO(end_date_formatted);
    addSingleQuotes(end_date_formatted);
    expense_buffers->end_date = end_date_formatted;

    freeExpenseBuffers(expense_buffers);
    loadExpenses(expense_buffers);
  }
}

void clearDateFilterFromExpenses(struct ExpenseBuffers *expense_buffers, char *start_date, char *end_date){
  int isFiltered = strcmp(expense_buffers->start_date, "(SELECT MIN(date) FROM expenses)");
  if (isFiltered){ 
    expense_buffers->start_date = "(SELECT MIN(date) FROM expenses)";
    expense_buffers->end_date = "(SELECT MAX(date) FROM expenses)";
    freeExpenseBuffers(expense_buffers);
    loadExpenses(expense_buffers);
    memset(start_date, '\0', strlen(start_date));
    memset(end_date, '\0', strlen(end_date));
  } 
}

void applyDateFilterToTotals(struct TotalBuffers *total_buffers, char *start_date, char *end_date){
  static char start_date_formatted[DATE_SIZE + 2], end_date_formatted[DATE_SIZE + 2];
  strcpy(start_date_formatted, start_date);
  strcpy(end_date_formatted, end_date);
  if (checkFilterDate(start_date_formatted) && checkFilterDate(end_date_formatted)){
    formatDateToISO(start_date_formatted);
    addSingleQuotes(start_date_formatted);
    total_buffers->start_date = start_date_formatted;
    
    formatDateToISO(end_date_formatted);
    addSingleQuotes(end_date_formatted);
    total_buffers->end_date = end_date_formatted;
    
    freeTotalBuffers(total_buffers);
    setStats(total_buffers);
    loadTotals(total_buffers);
  }
}

void clearDateFilterFromTotals(struct TotalBuffers *total_buffers, char *start_date, char *end_date){
  int isFiltered = strcmp(total_buffers->start_date, "(SELECT MIN(date) FROM expenses)");
  if (isFiltered){ 
    total_buffers->start_date = "(SELECT MIN(date) FROM expenses)";
    total_buffers->end_date = "(SELECT MAX(date) FROM expenses)";
    freeTotalBuffers(total_buffers);
    setStats(total_buffers);
    loadTotals(total_buffers);
    memset(start_date, '\0', strlen(start_date));
    memset(end_date, '\0', strlen(end_date));
  } 
}
