#ifndef dbs_h
#define dbs_h

#include "lib/sqlite3.h"

void initDb(void);
void createTable(void);

void addExpense(char *date, char *name, double price, const char *tag);
int getFields(char *date, char *name, double *price, char *tag, int id);
void amendExpense(char *date, char *name, double price, const char *tag, int id);
void eraseExpense(int id);

void eraseAllExpenses(void);

void getExpensesCount(int *out, char *name, char *start_date, char *end_date);
void getUsedTagsCount(int *out, char *start_date, char *end_date);
void getTotalSum(double *out, char *start_date, char *end_date);
void getTotalCount(int *out, char *start_date, char *end_date);

sqlite3_stmt* prepGetExpense(int field, int chosen_order, char *name, char *start_date, char *end_date);
void nextExpense(sqlite3_stmt *expense_stmt);
int getID(sqlite3_stmt *expense_stmt);
const char* getDate(sqlite3_stmt *expense_stmt);
const char* getName(sqlite3_stmt *expense_stmt);
double getPrice(sqlite3_stmt *expense_stmt);
const char* getTag(sqlite3_stmt *expense_stmt);
void finalizeExpenseStmt(sqlite3_stmt *expense_stmt);

sqlite3_stmt* prepTotals(char *start_date, char *end_date);
void nextTotal(sqlite3_stmt *total_stmt);
const char* getGroupedTag(sqlite3_stmt *total_stmt);
double getGroupedSum(sqlite3_stmt *total_stmt);
int getGroupedCount(sqlite3_stmt *total_stmt);
void finalizeTotalStmt(sqlite3_stmt *total_stmt);

void closeDb(void);

#endif
