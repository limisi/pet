#ifndef item_h
#define item_h

#include "itemSizes.h"// ID_SIZE, DATE_SIZE, NAME_SIZE, PRICE_SIZE, TAG_SIZE
#define EXTRA_SPACE 8

enum {INPUT_OK, BLANK_FIELD, INVALID_DATE_FMT, INVALID_DATE, NOT_EXIST, GET_DATA_NOT_PRESSED};

struct Item{
  int id;
  char date[DATE_SIZE + EXTRA_SPACE];// EXTRA_SPACE used to accomodate chooseDate widget
  char name[NAME_SIZE];
  char price_str[PRICE_SIZE + EXTRA_SPACE];// EXTRA_SPACE used to accomodate sprintf copy precision
  double price;
  int tag;
  int result_code;

  //used only in updateExpense and deleteExpense
  char id_str[ID_SIZE]; 
  char tag_str[TAG_SIZE];
  int id_found;
  int get_data_pressed;
};

int checkInputBuffers(struct Item *item);
int checkUpdateBuffers(struct Item *item);

void clearItemTextboxes(struct Item *item);

void clearIdTextbox(struct Item *item);
void clearUpdateTextboxes(struct Item *item);
void clearDeleteLabels(struct Item *item);

#endif
