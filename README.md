# pet
An app to keep track of your expenses.

# Features
- Lightweight (release build is ~2MB, memory usage is ~25MB)
- GPU accelerated via OpenGL
- Cross-platform (tested on Windows and Linux)
- Open source forever
- Password authentication
- Simple
- Support for decimal point currencies
- Descriptive error messages
- Basic CRUD operations
- Totals 

# Demo
[![pet v2.5](https://markdown-videos-api.jorgenkh.no/url?url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DPj-LODYVz14)](https://www.youtube.com/watch?v=Pj-LODYVz14)

# Dependancies
- OpenGL 3.3+ via [glad](https://github.com/Dav1dde/glad)
- [SQLite3](https://www.sqlite.org/index.html) for the database
- [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear) for the UI layout
- [GLFW](https://github.com/glfw/glfw) for windowing
- [QOI](https://github.com/phoboslab/qoi) for icon loading
- [MPack](https://github.com/ludocode/mpack) for serialization
- [Argon2](https://github.com/P-H-C/phc-winner-argon2) for password authentication

# Building
**prerequisites**
1. Have a C compiler installed.
2. Install [meson](https://mesonbuild.com/SimpleStart.html#installing-meson).
3. Clone the repository: ``git clone https://codeberg.org/limisi/pet.git``

From a terminal, cd /root/of/pet then run the following commands:
```
mkdir subprojects
meson wrap install glfw
meson setup debugbuild
meson compile -C debugbuild
```

**running**
```
cd debugbuild
./pet
```
# Motivation
Whilst transitioning from youth to adulthood, I became responsible for my finances 
and shortly after noticed that my expenditure was rather fluctuant.
Therefore there was a need to keep better track of my expenses, so I believe that 
this program shall help anyone become more financially responsible especially the youth
transitioning to adulthood, thank you. And I'm just wondering why sum like this didn' already exist!!??

# Acknowledgement
I wish to thank the following:
- Richard Hipp and contributers for SQLite3
- Micha Mettke and maintainers for Nuklear
- Marcus Geelnard, Camilla Löwy and contributers for GLFW
- David Herberth and contributors for glad
- Dominic Szablewski and contributors for QOI
- Nicholas Fraser and authors for MPack
- Daniel Dinu, Dmitry Khovratovich, Jean-Philippe Aumasson, and Samuel Neves for Argon2

Without whose work, pet would not have been possible, thank you.

I also wish to thank the kind folks over at [TPH](https://theprogrammershangout.com/) for the pointers and guidance they offered me, thank you.

And what is icon loader without an icon to load? Big Thank you to Michael Magu for designing pet's bold and majestic icon.

Finally, big thank you to Google for NotoSans which is a really beautiful font.

# Why the name pet?
I'm glad you asked, Personal Expenses Tracker.

# License
This software is licensed under the permissive [zlib license](https://codeberg.org/limisi/pet/src/branch/master/LICENSE).
