#include "lib/mpack.h"// file flushing and loading
#include "conf.h"
#include "paths.h"

void loadConfigurations(struct Conf *conf){
  mpack_tree_t dummy_tree;
  mpack_tree_t *tree = &dummy_tree;
  mpack_tree_init_filename(tree, CONF_FILE_PATH, 0);
  mpack_tree_parse(tree);
  mpack_node_t root = mpack_tree_root(tree);
  conf->theme = mpack_node_int(mpack_node_map_cstr(root,"theme"));
  conf->password_set = mpack_node_int(mpack_node_map_cstr(root,"password"));
  conf->encoded_length = mpack_node_int(mpack_node_map_cstr(root,"encoded length"));
  mpack_node_copy_utf8_cstr(mpack_node_map_cstr(root,"name"), conf->name, USERNAME_CAPACITY);
  mpack_node_copy_utf8_cstr(mpack_node_map_cstr(root,"hash"), conf->hash, HASH_CAPACITY);
  mpack_node_copy_utf8_cstr(mpack_node_map_cstr(root,"salt"), conf->salt, SALT_CAPACITY);
  if (mpack_tree_destroy(tree) != mpack_ok) 
    printf("Mpack: tree error in loadConfigurations\n");
}

void saveConfigurations(struct Conf *conf){
  mpack_writer_t dummy_writer;
  mpack_writer_t *writer = &dummy_writer;
  mpack_writer_init_filename(writer, CONF_FILE_PATH);
  mpack_start_map(writer, 6);
  
  mpack_write_utf8_cstr(writer, "theme");
  mpack_write_int(writer, conf->theme);
  
  mpack_write_utf8_cstr(writer, "password");
  mpack_write_int(writer, conf->password_set);

  mpack_write_utf8_cstr(writer, "encoded length");
  mpack_write_int(writer, conf->encoded_length);

  mpack_write_utf8_cstr(writer, "name");
  mpack_write_utf8_cstr(writer, conf->name);

  mpack_write_utf8_cstr(writer, "hash");
  mpack_write_utf8_cstr(writer, conf->hash);
  
  mpack_write_utf8_cstr(writer, "salt");
  mpack_write_utf8_cstr(writer, conf->salt);
  
  mpack_finish_map(writer);
  if (mpack_writer_destroy(writer) != mpack_ok) 
    printf("Mpack: writer error in saveConfigurations\n");
}
