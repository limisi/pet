#ifndef paths_h
#define paths_h

#define PATH_PREFIX "../data/"

#define CONF_FILE_PATH PATH_PREFIX "conf.mp"
#define TAGS_FILE_PATH PATH_PREFIX "tags.mp"
#define DATABASE_FILE_PATH PATH_PREFIX "accounts.db" 
#define FONT_FILE_PATH PATH_PREFIX "NotoSans-Regular.ttf"

#define PETICON16_PATH PATH_PREFIX "peticon16.qoi"
#define PETICON32_PATH PATH_PREFIX "peticon32.qoi"
#define PETICON48_PATH PATH_PREFIX "peticon48.qoi"
#define PETICON64_PATH PATH_PREFIX "peticon64.qoi"
#define PETICON128_PATH PATH_PREFIX "peticon128.qoi"
#define PETICON256_PATH PATH_PREFIX "peticon256.qoi"

#endif
