#include "dbs.h"
#include <string.h>//strlen in sqlite3_prepare_v2, strcpy in getFields
#include <stdio.h>//sprintf in prepGetExpense
#include "paths.h"

#define GET_FIELDS_OK 1
#define GET_FIELDS_NONE 0

sqlite3 *db;

void initDb(void){
  sqlite3_open_v2(DATABASE_FILE_PATH, &db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE, NULL);
}

void createTable(void){
  const char *sql_stmt, *next_stmt;
  sqlite3_stmt *stmt;
  sql_stmt = "CREATE TABLE IF NOT EXISTS expenses\
	     (id INTEGER PRIMARY KEY, date TEXT,\
	      name TEXT, price REAL, tag TEXT);";
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

void addExpense(char *date, char *name, double price, const char *tag){
  const char *sql_stmt, *next_stmt;
  sqlite3_stmt *stmt;
  sql_stmt = "INSERT INTO expenses (date, name, price, tag)\
	     VALUES (:date, :name, :price, :tag);";
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_bind_text(stmt, 1, date, -1, SQLITE_STATIC);
  sqlite3_bind_text(stmt, 2, name, -1, SQLITE_STATIC);
  sqlite3_bind_double(stmt, 3, price);
  sqlite3_bind_text(stmt, 4, tag, -1, SQLITE_STATIC);
  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

int getFields(char *date, char *name, double *price, char *tag, int id){
  const char *sql_stmt, *next_stmt;
  sqlite3_stmt *stmt;
  sql_stmt = "SELECT date,name,price,tag FROM expenses WHERE id=:id;";
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_bind_int(stmt, 1, id);
  if (sqlite3_step(stmt) == SQLITE_ROW){ 
    strcpy(date, (const char*)sqlite3_column_text(stmt, 0));
    strcpy(name, (const char*)sqlite3_column_text(stmt, 1));
    *price = sqlite3_column_double(stmt, 2);
    strcpy(tag, (const char*)sqlite3_column_text(stmt, 3));
    sqlite3_finalize(stmt);
    return GET_FIELDS_OK;
  } else {
    sqlite3_finalize(stmt);
    return GET_FIELDS_NONE;
  }
}

void amendExpense(char *date, char *name, double price, const char *tag, int id){
  const char *sql_stmt, *next_stmt;
  sqlite3_stmt *stmt;
  sql_stmt = "UPDATE expenses SET date=:date, name=:name, price=:price, tag=:tag WHERE id=:id;";
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_bind_text(stmt, 1, date, -1, SQLITE_STATIC);
  sqlite3_bind_text(stmt, 2, name, -1, SQLITE_STATIC);
  sqlite3_bind_double(stmt, 3, price);
  sqlite3_bind_text(stmt, 4, tag, -1, SQLITE_STATIC);
  sqlite3_bind_int(stmt, 5, id);
  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

void eraseExpense(int id){
  const char *sql_stmt, *next_stmt;
  sqlite3_stmt *stmt;
  sql_stmt = "DELETE FROM expenses WHERE id=:id;";
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_bind_int(stmt, 1, id);
  sqlite3_step(stmt); 
  sqlite3_finalize(stmt);
}

void eraseAllExpenses(void){
  const char *sql_stmt, *next_stmt;
  sqlite3_stmt *stmt;
  sql_stmt = "DELETE FROM expenses;";
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

void getExpensesCount(int *out, char *name, char *start_date, char *end_date){
  char sql_stmt[400];
  const char *next_stmt;
  sqlite3_stmt *stmt;
  sprintf(sql_stmt, "SELECT COUNT() FROM expenses WHERE name LIKE %s AND date BETWEEN %s AND %s;", name, start_date, end_date);
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_step(stmt);
  *out = sqlite3_column_int(stmt, 0); 
  sqlite3_finalize(stmt);
}

void getUsedTagsCount(int *out, char *start_date, char *end_date){
  char sql_stmt[160];
  sqlite3_stmt *stmt;
  const char *next_stmt;
  sprintf(sql_stmt, "SELECT COUNT() FROM (SELECT SUM(price) FROM expenses WHERE date BETWEEN %s AND %s GROUP BY tag);", start_date, end_date);
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_step(stmt);
  *out = sqlite3_column_int(stmt, 0); 
  sqlite3_finalize(stmt);
}

void getTotalSum(double *out, char *start_date, char *end_date){
  char sql_stmt[130];
  sqlite3_stmt *stmt;
  const char *next_stmt;
  sprintf(sql_stmt, "SELECT SUM(price) FROM expenses WHERE date BETWEEN %s AND %s;", start_date, end_date);
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_step(stmt);
  *out = sqlite3_column_double(stmt, 0); 
  sqlite3_finalize(stmt);
}

void getTotalCount(int *out, char *start_date, char *end_date){
  char sql_stmt[130];
  sqlite3_stmt *stmt;
  const char *next_stmt;
  sprintf(sql_stmt, "SELECT COUNT() FROM expenses WHERE date BETWEEN %s AND %s;", start_date, end_date);
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&stmt,&next_stmt);
  sqlite3_step(stmt);
  *out = sqlite3_column_int(stmt, 0); 
  sqlite3_finalize(stmt);
}

sqlite3_stmt* prepGetExpense(int field, int chosen_order, char *name, char *start_date, char *end_date){
  char sql_stmt[400];
  const char *next_stmt;
  sqlite3_stmt *expense_stmt;
  char *column = "id", *sequence = "ASC";
  switch(field){ 
    case 0: column = "date"; break; 
    case 1: column = "name"; break;
    case 2: column = "price"; break;
    case 3: column = "tag"; break;
  }
  switch(field){
    case 0: case 2: sequence = chosen_order == 0 ? "DESC" : "ASC"; break;
    case 1: case 3: sequence = chosen_order == 0 ? "ASC" : "DESC"; break;
  }
  sprintf(sql_stmt, "SELECT * FROM expenses WHERE name LIKE %s AND date BETWEEN %s AND %s ORDER BY %s %s;", name, start_date, end_date, column, sequence);
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&expense_stmt,&next_stmt);
  return expense_stmt;
}

sqlite3_stmt* prepTotals(char *start_date, char *end_date){
  char sql_stmt[200];
  const char *next_stmt;
  sqlite3_stmt *total_stmt;
  sprintf(sql_stmt, "SELECT tag, SUM(price) as total, COUNT(name) FROM expenses WHERE date BETWEEN %s AND %s GROUP BY tag ORDER BY total DESC;", start_date, end_date); 
  sqlite3_prepare_v2(db,sql_stmt,strlen(sql_stmt)+1,&total_stmt,&next_stmt);
  return total_stmt;
}

int getID(sqlite3_stmt *expense_stmt){
  return sqlite3_column_int(expense_stmt, 0);
}

const char* getDate(sqlite3_stmt *expense_stmt){
  return (const char*)sqlite3_column_text(expense_stmt, 1);
}

const char* getName(sqlite3_stmt *expense_stmt){
  return (const char*)sqlite3_column_text(expense_stmt, 2);
}

double getPrice(sqlite3_stmt *expense_stmt){
  return sqlite3_column_double(expense_stmt, 3);
}

const char* getTag(sqlite3_stmt *expense_stmt){
  return (const char*)sqlite3_column_text(expense_stmt, 4);
}

const char* getGroupedTag(sqlite3_stmt *total_stmt){
  return (const char*)sqlite3_column_text(total_stmt, 0);
}

double getGroupedSum(sqlite3_stmt *total_stmt){
  return sqlite3_column_double(total_stmt, 1);
}

int getGroupedCount(sqlite3_stmt *total_stmt){
  return sqlite3_column_double(total_stmt, 2);
}

void nextExpense(sqlite3_stmt *expense_stmt){
  sqlite3_step(expense_stmt);
}

void nextTotal(sqlite3_stmt *total_stmt){
  sqlite3_step(total_stmt);
}

void finalizeExpenseStmt(sqlite3_stmt *expense_stmt){
  sqlite3_finalize(expense_stmt);
}

void finalizeTotalStmt(sqlite3_stmt *total_stmt){
  sqlite3_finalize(total_stmt);
}

void closeDb(void){
  sqlite3_close_v2(db);
}
