#ifndef load_h
#define load_h

struct ExpenseBuffers{
  int *ids;
  char *dates;
  char *names;
  char **name_ptrs;
  double *prices;
  char *tags;
  char **tag_ptrs;
  int expenses_count;
  int chosen_order;
  int chosen_field;
  char *start_date;
  char *end_date;
  char *filter_name;
};

struct TotalBuffers{
  char *grouped_tags;
  char **grouped_tag_ptrs;
  double *grouped_sums;
  int *grouped_counts;
  int used_tags_count;
  int expenses_count;
  double total_sum;
  char *start_date;
  char *end_date;
};

void setStats(struct TotalBuffers *total_buffers);
void allocateExpenseBuffers(struct ExpenseBuffers *record_buffers);
void loadExpenses(struct ExpenseBuffers *record_buffers);
void freeExpenseBuffers(struct ExpenseBuffers *record_buffers);
void allocateTotalBuffers(struct TotalBuffers *total_buffers);
void loadTotals(struct TotalBuffers *total_buffers);
void freeTotalBuffers(struct TotalBuffers *total_buffers);
void reloadExpensesAndTotals(struct ExpenseBuffers *record_buffers, struct TotalBuffers *total_buffers);

#endif
