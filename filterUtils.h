#ifndef filterUtils_h
#define filterUtils_h

#include "load.h"

int checkFilterDate(char *filter_date);
void addPercentSigns(char *name);
void addSingleQuotes(char *string);

void applyNameFilterToExpenses(struct ExpenseBuffers *expense_buffers, char *filter_name);
void clearNameFilterFromExpenses(struct ExpenseBuffers *expense_buffers, char *filter_name);

void applyDateFilterToExpenses(struct ExpenseBuffers *expense_buffers, char *start_date, char *end_date);
void clearDateFilterFromExpenses(struct ExpenseBuffers *expense_buffers, char *start_date, char *end_date);

void applyDateFilterToTotals(struct TotalBuffers *total_buffers, char *start_date, char *end_date);
void clearDateFilterFromTotals(struct TotalBuffers *total_buffers, char *start_date, char *end_date);

#endif
