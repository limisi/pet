#include <stdio.h>
#include <string.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_KEYSTATE_BASED_INPUT
#include "lib/nuklear.h"
#include "passwordUI.h"
#include "passwordUtils.h"
#include "conf.h"
#include "passwordConstants.h"

void nameErrorLabels(struct nk_context *ctx, int *name_result_code){
  if (*name_result_code != NAME_OK){
    static char *issue = "[PLACE_HOLDER]";
    switch (*name_result_code){
      case EMPTY_NAME:
        issue = "Name is empty";
        break;
    }
    static const float widget_sizes[] = {175, 70};
    nk_layout_row(ctx, NK_STATIC, 40, 2, widget_sizes);
    nk_label(ctx, issue, NK_TEXT_LEFT);
    if (nk_button_label(ctx, "Ok")) 
      *name_result_code = NAME_OK;
  }
}

void passwordErrorLabels(struct nk_context *ctx, int *password_result_code){
  if (*password_result_code != PASSWORD_OK ){ 
    static int label_width = 0;
    static char *issue = "[PLACE_HOLDER]";
    switch(*password_result_code){
      case TOO_SHORT:
        label_width = 250;
        issue = "Password is too short";
        break;
      case NO_DIGIT:
        label_width = 250;
        issue = "Password lacks a digit";
        break;
      case NO_SYMBOL:
        label_width = 280;
        issue = "Password lacks a symbol";
        break;
      case NO_UPPER:
        label_width = 400;
        issue = "Password lacks an uppercase letter";
        break;
      case NO_LOWER:
        label_width = 380;
        issue = "Password lacks a lowercase letter";
        break;
      case NO_MATCH:
        label_width = 300;
        issue = "Password does not match";
        break;
      case WRONG_PASSWORD:
        label_width = 220;
        issue = "Incorrect password";
        break;
      case PASSWORD_CHANGED:
        label_width = 220;
        issue = "Change successful";
        break;
    }   
    static float widget_sizes[2]; 
    widget_sizes[0] = label_width;
    widget_sizes[1] = 70;
    nk_layout_row(ctx, NK_STATIC, 40, 2, widget_sizes);
    nk_label(ctx, issue, NK_TEXT_LEFT);
    if (nk_button_label(ctx, "Ok")) 
      *password_result_code = PASSWORD_OK;
  }
}

void setPasswordWindow(struct nk_context *ctx, struct Conf *conf, struct UIState *ui_state){
  static char name[USERNAME_CAPACITY];
  
  static char password[PASSWORD_CAPACITY];
  static char true_password[PASSWORD_CAPACITY];
  static int password_old_len = 0;
  static int password_current_len = 0;
  
  static char confirmation[PASSWORD_CAPACITY];
  static char true_confirmation[PASSWORD_CAPACITY];
  static int confirmation_old_len = 0;
  static int confirmation_current_len = 0;

  static int name_result_code = NAME_OK;
  static int password_result_code = PASSWORD_OK;

  static const float widget_sizes[] = {0.35, 0.55};
  static const float help_widget_size[] = {140};

  nk_layout_row_dynamic(ctx, 30, 1);
  nk_label(ctx, "WELCOME TO PET :)", NK_TEXT_CENTERED);
  nk_label(ctx, "Get started by setting a password :)", NK_TEXT_CENTERED);
  
  nk_layout_row(ctx, NK_STATIC, 40, 1, help_widget_size);
  if (nk_widget_is_hovered(ctx)){
    nk_tooltip_begin(ctx, 450);
    nk_layout_row_dynamic(ctx, 25, 1);
    nk_label(ctx, "Your password MUST:", NK_TEXT_LEFT);
    nk_labelf(ctx, NK_TEXT_LEFT, "1) Be at least %d characters long", MINIMUM_PASSWORD_LENGTH);
    nk_label(ctx, "2) Contain at least 1 lowercase letter", NK_TEXT_LEFT);
    nk_label(ctx, "3) Contain at least 1 uppercase letter", NK_TEXT_LEFT);
    nk_label(ctx, "4) Contain at least 1 digit", NK_TEXT_LEFT);
    nk_label(ctx, "5) Contain at least 1 symbol", NK_TEXT_LEFT);
    nk_tooltip_end(ctx);
  }
  nk_label(ctx, "SHOW HELP", NK_TEXT_LEFT);
  passwordErrorLabels(ctx, &password_result_code);
  nameErrorLabels(ctx, &name_result_code);
 
  nk_layout_row(ctx, NK_DYNAMIC, 40, 2, widget_sizes);
  nk_label(ctx, "Enter your name", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, name, USERNAME_CAPACITY - 1, nk_filter_default);

  nk_label(ctx, "Enter a password", NK_TEXT_LEFT);
  obfuscatePassword(password, '*', &password_old_len, password_current_len);
  nk_edit_string(ctx, NK_EDIT_SIMPLE, password, &password_current_len, PASSWORD_CAPACITY - 1, nk_filter_default);
  copyPlainPassword(true_password, password, password_old_len, password_current_len);

  nk_label(ctx, "Confirm password", NK_TEXT_LEFT);
  obfuscatePassword(confirmation, '*', &confirmation_old_len, confirmation_current_len);
  nk_edit_string(ctx, NK_EDIT_SIMPLE, confirmation, &confirmation_current_len, PASSWORD_CAPACITY - 1, nk_filter_default);
  copyPlainPassword(true_confirmation, confirmation, confirmation_old_len, confirmation_current_len);

  nk_label(ctx, "", NK_TEXT_LEFT);
  if (nk_button_label(ctx, "Set Password")){
    password_result_code = checkPassword(true_password, true_confirmation, password_current_len);
    name_result_code = checkName(name);
    if (password_result_code != PASSWORD_OK){  
      clearPasswordBuffer(true_password, &password_current_len);
      clearPasswordBuffer(true_confirmation, &confirmation_current_len);
    }
    else if (password_result_code == PASSWORD_OK && name_result_code == NAME_OK){
      saveName(name, conf);
      generateSalt(conf->salt);
      hashPassword(true_password, password_current_len, conf); 
      flagPasswordAsSet(conf);
      proceedToLogin(ui_state);
      clearPasswordBuffer(true_password, &password_current_len);
      clearPasswordBuffer(true_confirmation, &confirmation_current_len);
    }
  }
}

void proceedToLogin(struct UIState *ui_state){
  ui_state->show_set_password_ui = 0;
  ui_state->show_enter_password_ui = 1;
}

void enterPasswordWindow(struct nk_context *ctx, struct Conf *conf, struct UIState *ui_state){
  static int password_result_code = PASSWORD_OK;
  static char password[PASSWORD_CAPACITY];
  static char true_password[PASSWORD_CAPACITY];
  static int password_old_len = 0;
  static int password_current_len = 0;
  static const float widget_sizes[] = {0.28, 0.55};
  
  nk_layout_row_dynamic(ctx, 30, 1);
  nk_labelf(ctx, NK_TEXT_CENTERED, "Welcome %s :)", conf->name);
  
  passwordErrorLabels(ctx, &password_result_code);
  nk_layout_row(ctx, NK_DYNAMIC, 40, 2, widget_sizes);
  
  nk_label(ctx, "Enter Password:", NK_TEXT_LEFT);
  obfuscatePassword(password, '*', &password_old_len, password_current_len);
  nk_edit_string(ctx, NK_EDIT_SIMPLE, password, &password_current_len, PASSWORD_CAPACITY - 1, nk_filter_default);
  copyPlainPassword(true_password, password, password_old_len, password_current_len);

  nk_label(ctx, "", NK_TEXT_LEFT);
  if (nk_button_label(ctx, "Login")){
    password_result_code = verifyPassword(conf, true_password, password_current_len);
    if (password_result_code == WRONG_PASSWORD) clearPasswordBuffer(true_password, &password_current_len);
    else {
      proceedToExpensesFromLogin(ui_state);
      clearPasswordBuffer(true_password, &password_current_len);
    }
  }
}

void proceedToExpensesFromLogin(struct UIState *ui_state){
  ui_state->show_enter_password_ui = 0;
  ui_state->show_expenses_ui = 1;
  ui_state->expenses_loop_count = 0;
}

void changePasswordWindow(struct nk_context *ctx, struct Conf *conf, struct UIState *ui_state){
  static int password_result_code = PASSWORD_OK;

  static char prev_password[PASSWORD_CAPACITY];
  static char prev_true_password[PASSWORD_CAPACITY];
  static int prev_password_old_len = 0;
  static int prev_password_current_len = 0;
  
  static char password[PASSWORD_CAPACITY];
  static char true_password[PASSWORD_CAPACITY];
  static int password_old_len = 0;
  static int password_current_len = 0;
  
  static char confirmation[PASSWORD_CAPACITY];
  static char true_confirmation[PASSWORD_CAPACITY];
  static int confirmation_old_len = 0;
  static int confirmation_current_len = 0;
  
  passwordErrorLabels(ctx, &password_result_code);

  static const float widget_sizes[] = {0.40, 0.55};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 2, widget_sizes);

  nk_label(ctx, "Enter old password", NK_TEXT_LEFT);
  obfuscatePassword(prev_password, '*', &prev_password_old_len, prev_password_current_len);
  nk_edit_string(ctx, NK_EDIT_SIMPLE, prev_password, &prev_password_current_len, PASSWORD_CAPACITY - 1, nk_filter_default);
  copyPlainPassword(prev_true_password, prev_password, prev_password_old_len, prev_password_current_len);
  
  nk_label(ctx, "Enter new password", NK_TEXT_LEFT);
  obfuscatePassword(password, '*', &password_old_len, password_current_len);
  nk_edit_string(ctx, NK_EDIT_SIMPLE, password, &password_current_len, PASSWORD_CAPACITY - 1, nk_filter_default);
  copyPlainPassword(true_password, password, password_old_len, password_current_len);
  
  nk_label(ctx, "Confirm new password", NK_TEXT_LEFT);
  obfuscatePassword(confirmation, '*', &confirmation_old_len, confirmation_current_len);
  nk_edit_string(ctx, NK_EDIT_SIMPLE, confirmation, &confirmation_current_len, PASSWORD_CAPACITY - 1, nk_filter_default);
  copyPlainPassword(true_confirmation, confirmation, confirmation_old_len, confirmation_current_len);

  nk_label(ctx, "", NK_TEXT_LEFT);
  if (nk_button_label(ctx, "Change Password")) {
    int check_result_code = checkPassword(true_password, true_confirmation, password_current_len);
    int verify_result_code = verifyPassword(conf, prev_true_password, prev_password_current_len);
    password_result_code = check_result_code > verify_result_code ? check_result_code : verify_result_code;
    if (password_result_code != PASSWORD_OK){  
      clearPasswordBuffer(true_password, &password_current_len);
      clearPasswordBuffer(true_confirmation, &confirmation_current_len);
    }
    else if (password_result_code == PASSWORD_OK){
      hashPassword(true_password, password_current_len, conf); 
      informSuccessfulPasswordChange(&password_result_code);
      clearPasswordBuffer(true_password, &password_current_len);
      clearPasswordBuffer(prev_true_password, &prev_password_current_len);
      clearPasswordBuffer(true_confirmation, &confirmation_current_len);
    }
  }
  nk_label(ctx, "", NK_TEXT_LEFT);
  if (nk_button_label(ctx, "Go Back To Expenses")) returnToExpenses(ui_state);
}

void returnToExpenses(struct UIState *ui_state){
  ui_state->show_expenses_ui = 1;
  ui_state->expenses_loop_count = 0;
  ui_state->show_change_password_ui = 0;
}
