#include "dates.h"
#include <stdio.h>// sscanf (also in isDateValid), sprintf in formatDateToISO, unformatDateFromISO and padDate
#include <string.h>// strlen in isDateFormatValid
#include <ctype.h>// isdigit in isDateFormatValid

void padDate(char *date){
  int day = 0, mon = 0, year = 0;
  sscanf(date, "%d/%d/%d", &day, &mon, &year);
  sprintf(date, "%02d/%02d/%d", day, mon, year);
}

void formatDateToISO(char *date){
  int day = 0, mon = 0, year = 0;
  sscanf(date, "%d/%d/%d", &day, &mon, &year);
  sprintf(date, "%d-%02d-%02d", year, mon, day);
}

const char* unformatDateFromISO(const char *date){
  int day = 0, mon = 0, year = 0;
  sscanf(date, "%d-%d-%d", &year, &mon, &day);
  sprintf((char*)date, "%02d/%02d/%d", day, mon, year);
  return date;
}

bool isDateFormatValid(char *date){
  if (strlen(date) != 10) return false;
  if (date[2] != '/' || date[5] != '/') return false;
  for (int i = 0; i < 10; i++)
    if (i != 2 && i != 5 && !isdigit(date[i])) return false;
  return true;
}

bool isDateValid(char *date){
  int day = 0, mon = 0, year = 0;
  int last_day = 0;
  sscanf(date, "%d/%d/%d", &day, &mon, &year);
  if (mon > 12 || day > 31 || mon < 1 || day < 1) return false;
  switch (mon){
    case 1:case 3:case 5:case 7:case 8:case 10:case 12:
      last_day = 31; 
      break;
    case 4:case 6:case 9:case 11: 
      last_day = 30; 
      break;
    case 2: 
      if ((!(year % 4) && ((year % 100))) || !(year % 400)) last_day = 29;
      else last_day = 28;
      break;
  }
  if (day > last_day) return false;
  return true;
}
