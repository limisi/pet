#ifndef tagsUI_h
#define tagsUI_h

void collapseTagsWindowsOnFirstDraw(struct nk_context *ctx, struct UIState *ui_state);
void addTagsWindow(struct nk_context *ctx, struct FileTags *file_tags, struct UIState *ui_state);
void editTagsWindow(struct nk_context *ctx, struct FileTags *file_tags, struct UIState *ui_state);
void backToExpensesWindow(struct nk_context *ctx, struct UIState *ui_state);
void returnToExpensesFromTags(struct UIState *ui_state);

#endif
