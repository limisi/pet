#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_KEYSTATE_BASED_INPUT
#include "lib/nuklear.h"
#include "UIState.h"// UIState struct in backToExpensesWindow and collapseTagsWindowsOnFirstDraw
#include "itemSizes.h"// TAG_SIZE
#include "tagUtils.h"// saveNewTags, editTags, clearTagBuffer 
#include "tagsUI.h"

void collapseTagsWindowsOnFirstDraw(struct nk_context *ctx, struct UIState *ui_state){
  if (ui_state->tags_loop_count < 1){
    nk_window_collapse(ctx, "Add Tags", NK_MINIMIZED);
    nk_window_collapse(ctx, "Edit Tags", NK_MINIMIZED);
    ui_state->tags_loop_count++;
  }
}

void addTagsWindow(struct nk_context *ctx, struct FileTags *file_tags, struct UIState *ui_state){
  static char new_tags[NUMBER_OF_NEW_TAGS_BOXES][TAG_SIZE];

  static const float widget_sizes[] = {0.15, 0.70};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 2, widget_sizes);

  for (int i = 0; i < NUMBER_OF_NEW_TAGS_BOXES; i++){ 
    nk_label(ctx, "Tag:", NK_TEXT_LEFT);
    nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, new_tags[i], TAG_SIZE - 1, nk_filter_default);
  } 
  
  nk_label(ctx, "", NK_TEXT_LEFT);
  if (nk_button_label(ctx, "Add Tags")) saveNewTags(file_tags, new_tags);
}

void editTagsWindow(struct nk_context *ctx, struct FileTags *file_tags, struct UIState *ui_state){
  static const float widget_sizes[] = {0.10, 0.65, 0.05};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 3, widget_sizes);

  static int tag_length = 0;
  for (int i = 0; i < file_tags->tags_count; i++){ 
    nk_label(ctx, "Tag:", NK_TEXT_LEFT);
    nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, file_tags->editable_tags + tag_length, TAG_SIZE - 1, nk_filter_default);
    if (nk_button_symbol(ctx, NK_SYMBOL_X)) clearTagBuffer(file_tags->editable_tags + tag_length);
    tag_length += TAG_SIZE;
  }
  tag_length = 0;

  nk_label(ctx, "", NK_TEXT_LEFT);
  if (nk_button_label(ctx, "Edit Tags")) editTags(file_tags);
}

void returnToExpensesFromTags(struct UIState *ui_state){
  ui_state->show_tags_ui = 0;
  ui_state->show_expenses_ui = 1;
  ui_state->expenses_loop_count = 0;
}

void backToExpensesWindow(struct nk_context *ctx, struct UIState *ui_state){
  nk_layout_row_dynamic(ctx, 35, 1);
  if (nk_button_label(ctx, "Go Back To Expenses")) returnToExpensesFromTags(ui_state);
}


