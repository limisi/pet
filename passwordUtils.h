#ifndef passwordUtils_h
#define passwordUtils_h

#include "conf.h"

enum {PASSWORD_OK, TOO_SHORT, NO_DIGIT, NO_SYMBOL, NO_UPPER, NO_LOWER, NO_MATCH, WRONG_PASSWORD, PASSWORD_CHANGED};
enum {NAME_OK, EMPTY_NAME};

int checkName(char *name);
void saveName(char *name, struct Conf *conf);

int checkPassword(char *true_password, char *true_confirmation, int password_length);
void clearPasswordBuffer(char *true_password, int *password_current_len);

void obfuscatePassword(char *password, char symbol, int *old_length, int new_length);
void copyPlainPassword(char *true_password, char *false_buffer, int old_length, int current_length);

void generateSalt(char *salt);
void hashPassword(char *password, int password_len, struct Conf *conf);
void flagPasswordAsSet(struct Conf *conf);

int verifyPassword(struct Conf *conf, char *true_password, int password_length);

void informSuccessfulPasswordChange(int *password_result_code);

#endif

