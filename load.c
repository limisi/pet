#include "load.h"
#include "dates.h"
#include "dbs.h"
#include "itemSizes.h"// DATE_SIZE
#include <stdlib.h>//malloc, free
#include <string.h>//strcpy

void setStats(struct TotalBuffers *total_buffers){
  getTotalCount(&total_buffers->expenses_count, total_buffers->start_date, total_buffers->end_date);
  getTotalSum(&total_buffers->total_sum, total_buffers->start_date, total_buffers->end_date);
}

void allocateExpenseBuffers(struct ExpenseBuffers *expense_buffers){
  unsigned long int name_buffer_len = 0, tag_buffer_len = 0;
  getExpensesCount(&expense_buffers->expenses_count, expense_buffers->filter_name, expense_buffers->start_date, expense_buffers->end_date);
  sqlite3_stmt *expense_stmt = prepGetExpense(expense_buffers->chosen_field, expense_buffers->chosen_order, expense_buffers->filter_name, expense_buffers->start_date, expense_buffers->end_date);
  for (int i = 0; i < expense_buffers->expenses_count; i++){ 
    nextExpense(expense_stmt);
    name_buffer_len += strlen(getName(expense_stmt));
    tag_buffer_len += strlen(getTag(expense_stmt));
  }
  finalizeExpenseStmt(expense_stmt);
  expense_buffers->ids = malloc(sizeof(int) * expense_buffers->expenses_count);
  expense_buffers->dates = malloc(DATE_SIZE * expense_buffers->expenses_count);
  expense_buffers->names = malloc(name_buffer_len + expense_buffers->expenses_count);
  expense_buffers->name_ptrs = malloc(sizeof(char**) * expense_buffers->expenses_count);
  expense_buffers->prices = malloc(sizeof(double) * expense_buffers->expenses_count);
  expense_buffers->tags = malloc(tag_buffer_len + expense_buffers->expenses_count);
  expense_buffers->tag_ptrs = malloc(sizeof(char**) * expense_buffers->expenses_count);
}

void loadExpenses(struct ExpenseBuffers *expense_buffers){
  allocateExpenseBuffers(expense_buffers);
  unsigned long int step_date = 0, step_name = 0, step_tag = 0;
  sqlite3_stmt *expense_stmt = prepGetExpense(expense_buffers->chosen_field, expense_buffers->chosen_order, expense_buffers->filter_name, expense_buffers->start_date, expense_buffers->end_date);
  for (int i = 0; i < expense_buffers->expenses_count; i++){ 
    nextExpense(expense_stmt);

    expense_buffers->ids[i] = getID(expense_stmt);
    
    strcpy(expense_buffers->dates + step_date, unformatDateFromISO(getDate(expense_stmt)));
    step_date += DATE_SIZE;
    
    strcpy(expense_buffers->names + step_name, getName(expense_stmt));
    expense_buffers->name_ptrs[i] = expense_buffers->names + step_name;
    step_name += strlen(expense_buffers->names + step_name) + 1;
    
    expense_buffers->prices[i] = getPrice(expense_stmt);
    
    strcpy(expense_buffers->tags + step_tag, getTag(expense_stmt));
    expense_buffers->tag_ptrs[i] = expense_buffers->tags + step_tag;
    step_tag += strlen(expense_buffers->tags + step_tag) + 1;
  }
  finalizeExpenseStmt(expense_stmt);
}

void freeExpenseBuffers(struct ExpenseBuffers *expense_buffers){
  free(expense_buffers->ids);
  free(expense_buffers->dates);
  free(expense_buffers->names);
  free(expense_buffers->name_ptrs);
  free(expense_buffers->prices);
  free(expense_buffers->tags);
  free(expense_buffers->tag_ptrs);
}

void allocateTotalBuffers(struct TotalBuffers *total_buffers){
  unsigned long int grouped_tag_buffer_len = 0;
  getUsedTagsCount(&total_buffers->used_tags_count, total_buffers->start_date, total_buffers->end_date);
  sqlite3_stmt *total_stmt = prepTotals(total_buffers->start_date, total_buffers->end_date);
  for (int i = 0; i < total_buffers->used_tags_count; i++){ 
    nextTotal(total_stmt);
    grouped_tag_buffer_len += strlen(getGroupedTag(total_stmt));
  }
  finalizeTotalStmt(total_stmt);
  total_buffers->grouped_tags = malloc(grouped_tag_buffer_len + total_buffers->used_tags_count);
  total_buffers->grouped_tag_ptrs = malloc(sizeof(char**) * total_buffers->used_tags_count);
  total_buffers->grouped_sums = malloc(sizeof(double) * total_buffers->used_tags_count);
  total_buffers->grouped_counts = malloc(sizeof(int) * total_buffers->used_tags_count);
}

void loadTotals(struct TotalBuffers *total_buffers){
  allocateTotalBuffers(total_buffers);
  unsigned long int step_grouped_tag = 0;
  sqlite3_stmt *total_stmt = prepTotals(total_buffers->start_date, total_buffers->end_date);
  for (int i = 0; i < total_buffers->used_tags_count; i++){ 
    nextTotal(total_stmt);
    
    strcpy(total_buffers->grouped_tags + step_grouped_tag, getGroupedTag(total_stmt));
    total_buffers->grouped_tag_ptrs[i] = total_buffers->grouped_tags + step_grouped_tag;
    step_grouped_tag += strlen(total_buffers->grouped_tags + step_grouped_tag) + 1;
    
    total_buffers->grouped_sums[i] = getGroupedSum(total_stmt);
    
    total_buffers->grouped_counts[i] = getGroupedCount(total_stmt);
  }
  finalizeTotalStmt(total_stmt);
}

void freeTotalBuffers(struct TotalBuffers *total_buffers){
  free(total_buffers->grouped_tags);
  free(total_buffers->grouped_tag_ptrs);
  free(total_buffers->grouped_sums);
  free(total_buffers->grouped_counts);
}

void reloadExpensesAndTotals(struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers){
  freeExpenseBuffers(expense_buffers);
  loadExpenses(expense_buffers);
  freeTotalBuffers(total_buffers);
  setStats(total_buffers);
  loadTotals(total_buffers);
}
