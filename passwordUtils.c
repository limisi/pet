#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "lib/argon2/argon2.h"
#include "passwordUtils.h"
#include "passwordConstants.h"

int checkName(char *name){
  if (strlen(name) == 0) return EMPTY_NAME;
  return NAME_OK;
}

void saveName(char *name, struct Conf *conf){
  strcpy(conf->name, name);
}

int checkPassword(char *password, char *confirm_password, int password_length){
  int symbol_present = 0, digit_present = 0;
  int uppercase_present = 0, lowercase_present = 0;
  if (password_length < MINIMUM_PASSWORD_LENGTH) return TOO_SHORT;
  for (int i = 0; i < password_length; i++){
    if(ispunct(password[i]) != 0) symbol_present = 1;
    if(isdigit(password[i]) != 0) digit_present = 1;
    if(isupper(password[i]) != 0) uppercase_present = 1;
    if(islower(password[i]) != 0) lowercase_present = 1;
  }
  if (symbol_present == 0) return NO_SYMBOL;
  if (digit_present == 0) return NO_DIGIT;
  if (uppercase_present == 0) return NO_UPPER;
  if (lowercase_present == 0) return NO_LOWER;
  if (strncmp(password, confirm_password, password_length) != 0) return NO_MATCH;
  return PASSWORD_OK;
}

void clearPasswordBuffer(char *true_password, int *password_current_len){
  memset(true_password, '\0', *password_current_len);
  *password_current_len = 0;
}

void obfuscatePassword(char *password, char symbol, int *old_length, int new_length){
  for (int i = 0; i < PASSWORD_CAPACITY; i++) 
    if ((password[i] & 128) == 0) password[i] = symbol;
  *old_length = new_length;
}

void copyPlainPassword(char *true_password, char *false_buffer, int old_length, int current_length){
  if (old_length < current_length) 
    memcpy(true_password + old_length, false_buffer + old_length, current_length - old_length);
}

void generateSalt(char *salt){ 
  srand(time(NULL));
  char charset[] = "9687VWXYghijABCDEFGHIJKklmnopqrstuvwxyzLMNOZ530124abcPQRSTUdef";
  int index = 0;
  for (int i = 0; i < SALT_LENGTH; i++) {
    index = rand() % CHARSET_LENGTH;
    salt[i] = charset[index];
  }
  salt[SALT_LENGTH] = '\0';
}

void hashPassword(char *password, int password_len, struct Conf *conf){
  uint32_t t_cost = 4;
  uint32_t m_cost = 1<<16;
  uint32_t parallelism = 2;
  argon2id_hash_encoded(t_cost, m_cost, parallelism, password, password_len, conf->salt, SALT_CAPACITY, HASH_LENGTH, conf->hash, HASH_CAPACITY);
  conf->encoded_length = argon2_encodedlen(t_cost, m_cost, parallelism, SALT_LENGTH, HASH_LENGTH, Argon2_id);
}

void flagPasswordAsSet(struct Conf *conf){
  conf->password_set = 1;
}

int verifyPassword(struct Conf *conf, char *true_password, int password_length){
  int result_code = argon2id_verify(conf->hash, true_password, password_length);
  if (result_code == ARGON2_OK) return PASSWORD_OK;
  else return WRONG_PASSWORD;
}

void informSuccessfulPasswordChange(int *password_result_code){
  *password_result_code = PASSWORD_CHANGED;
}
