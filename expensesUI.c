#include <stdio.h>//sprintf in chooseDate, getData and errorLabels
#include <time.h>//struct tm, time, localtime in chooseDate
#include <string.h>//memcpy in chooseDate
#include <stdlib.h>//atof in saveExpense and updateExpense, atoi in getData

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_KEYSTATE_BASED_INPUT
#include "lib/nuklear.h"// labels, buttons, layout, tooltip

#include "lib/style.h"//setStyle so as to changeTheme
#include "item.h"// checkInputBuffers, checkUpdateBuffers, clearItemTextboxes, clearIdTextbox, clearUpdateTextboxes, clearDeleteLabels
#include "load.h"// freeExpenseBuffers and loadExpenses in sortExpenses but mostly reloadExpensesAndTotals 
#include "itemSizes.h"// ID_SIZE,DATE_SIZE,NAME_SIZE,PRICE_SIZE,TAG_SIZE
#include "dates.h"// formatDateToISO in saveExpense and updateExpense, unformatDateFromISO in getData
#include "dbs.h"// getFields in getData, addExpense in saveExpense, amendExpense in updateExpense, eraseExpense and eraseAllExpenses in deleteExpense
#include "UIState.h"// UIState struct 
#include "conf.h"
#include "tagUtils.h"
#include "expensesUI.h"
#include "filterUtils.h"

#define NUMBER_OF_WEEKDAYS 7

void collapseExpensesWindowsOnFirstDraw(struct nk_context *ctx, struct UIState *ui_state){ 
  if (ui_state->expenses_loop_count < 1){ 
    nk_window_collapse(ctx, "View Expenses", NK_MINIMIZED);
    nk_window_collapse(ctx, "Input Expenses", NK_MINIMIZED);
    nk_window_collapse(ctx, "Update Expenses", NK_MINIMIZED);
    nk_window_collapse(ctx, "Delete Expenses", NK_MINIMIZED);
    nk_window_collapse(ctx, "Totals", NK_MINIMIZED);
    nk_window_collapse(ctx, "Settings", NK_MINIMIZED);
    ui_state->expenses_loop_count++;
  }
}

void errorLabels(struct nk_context *ctx, struct Item *item, char *action){
  if (item->result_code != INPUT_OK){ 
    static char msg[80];
    static int label_width = 0;
    static char *reason = "[PLACE_HOLDER]";
    switch(item->result_code){
      case BLANK_FIELD:
        label_width = 530;
        reason = "blank field(s)";
        break;
      case INVALID_DATE_FMT:
        label_width = 600;
        reason = "invalid date format";
        break;
      case INVALID_DATE:
        label_width = 530;
        reason = "invalid date";
        break;
      case NOT_EXIST:
        label_width = 550;
        reason = "it not existing";
        break;
      case GET_DATA_NOT_PRESSED:
        label_width = 650;
        reason = "not pressing 'get data'";
        break;
    }
    static float widget_sizes[2];
    widget_sizes[0] = label_width;
    widget_sizes[1] = 70;
    nk_layout_row(ctx, NK_STATIC, 40, 2, widget_sizes);
    sprintf(msg, "Error: Expense not %s due to %s", action, reason);
    nk_label(ctx, msg, NK_TEXT_LEFT);
    if (nk_button_label(ctx, "Ok"))
      item->result_code = INPUT_OK;
  }
}

void viewExpensesWindow(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers){ 
  static int show_menubar = 1;
  struct nk_rect window_bounds = nk_window_get_bounds(ctx);
  if (nk_contextual_begin(ctx, 0, nk_vec2(145, 50), window_bounds)){
    nk_layout_row_dynamic(ctx, 30, 1);
    nk_selectable_label(ctx, show_menubar?"Hide Filters":"Show Filters", NK_TEXT_LEFT, &show_menubar);
    nk_contextual_end(ctx);
  }
  if (show_menubar){
    nk_menubar_begin(ctx);
    nameFilterWidgetsInExpenses(ctx, expense_buffers);
    dateFilterWidgetsInExpenses(ctx, expense_buffers);
    sortExpenses(ctx, expense_buffers);
    nk_menubar_end(ctx);
  }      
  static unsigned long int step_date = 0; 
  static const float widget_sizes[] = {0.20, 0.30, 0.20, 0.30};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 4, widget_sizes);
  nk_label(ctx, "Date", NK_TEXT_LEFT);
  nk_label(ctx, "Name", NK_TEXT_LEFT);
  nk_label(ctx, "Price", NK_TEXT_LEFT);
  nk_label(ctx, "Tag", NK_TEXT_LEFT);
  for (int i = 0; i < expense_buffers->expenses_count; i++){  
    if (nk_widget_is_hovered(ctx)) nk_tooltipf(ctx, "Id: %d", expense_buffers->ids[i]);
    nk_labelf(ctx, NK_TEXT_LEFT, "%s", expense_buffers->dates + step_date);
    step_date += DATE_SIZE;
    
    if (nk_widget_is_hovered(ctx)) nk_tooltipf(ctx, "Id: %d", expense_buffers->ids[i]);
    nk_labelf(ctx, NK_TEXT_LEFT, "%s", expense_buffers->name_ptrs[i]);
    
    if (nk_widget_is_hovered(ctx)) nk_tooltipf(ctx, "Id: %d", expense_buffers->ids[i]);
    nk_labelf(ctx, NK_TEXT_LEFT, "%.13g", expense_buffers->prices[i]);
    
    nk_labelf(ctx, NK_TEXT_LEFT, "%s", expense_buffers->tag_ptrs[i]);
  }
  step_date = 0;
}

void nameFilterWidgetsInExpenses(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers){
  static char filter_name[NAME_SIZE];
  static const float name_widget_sizes[] = {0.20, 0.55, 0.15, 0.05};
  nk_layout_row(ctx, NK_DYNAMIC, 35, 4, name_widget_sizes);
  nk_label(ctx, "Name Filter:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, filter_name, NAME_SIZE - 1, nk_filter_default);
  if (nk_button_label(ctx, "Apply")) applyNameFilterToExpenses(expense_buffers, filter_name);
  if (nk_button_symbol(ctx, NK_SYMBOL_X)) clearNameFilterFromExpenses(expense_buffers, filter_name);
}

void dateFilterWidgetsInExpenses(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers){
  static char start_date[DATE_SIZE], end_date[DATE_SIZE];
  static const float date_filter_widget_sizes[] = {0.20, 0.25, 0.05, 0.25, 0.15, 0.05};
  nk_layout_row(ctx, NK_DYNAMIC, 35, 6, date_filter_widget_sizes);
  nk_label(ctx, "Date Filter:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, start_date, DATE_SIZE - 1, nk_filter_default);
  nk_label(ctx, "to", NK_TEXT_CENTERED);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, end_date, DATE_SIZE - 1, nk_filter_default);  
  if (nk_button_label(ctx, "Apply")) applyDateFilterToExpenses(expense_buffers, start_date, end_date);
  if (nk_button_symbol(ctx, NK_SYMBOL_X)) clearDateFilterFromExpenses(expense_buffers, start_date, end_date);
}

void sortExpenses(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers){
  static const char *fields[] = {"Date", "Name", "Price", "Tag"};
  static const char *order[] = {"newest", "oldest", "biggest", "smallest", "a-z", "z-a"};
  static unsigned short int step_order = 0;
  
  static const float sort_widget_sizes[] = {0.20, 0.20, 0.15, 0.20, 0.20};
  nk_layout_row(ctx, NK_DYNAMIC, 35, 5, sort_widget_sizes);

  nk_label(ctx, "Sort By:", NK_TEXT_LEFT);
  expense_buffers->chosen_field = nk_combo(ctx, fields, 4, expense_buffers->chosen_field, 25, nk_vec2(105,200));
 
  switch (expense_buffers->chosen_field){
    case 0: step_order = 0; break;
    case 1: step_order = 4; break;
    case 2: step_order = 2; break;
    case 3: step_order = 4; break;
  } 

  nk_label(ctx, "Order:", NK_TEXT_CENTERED);
  expense_buffers->chosen_order = nk_combo(ctx, order + step_order, 2, expense_buffers->chosen_order, 25, nk_vec2(130,200));

  if (nk_button_label(ctx, "Sort")){
    freeExpenseBuffers(expense_buffers);
    loadExpenses(expense_buffers);
  }
}

void inputExpensesWindow(struct nk_context *ctx, struct Item *item, struct FileTags *file_tags, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers){
  errorLabels(ctx, item, "saved");
  
  static const float widget_sizes[] = {0.15, 0.70};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 2, widget_sizes);

  nk_label(ctx, "Date:", NK_TEXT_LEFT);
  chooseDate(ctx, item->date);
  
  nk_label(ctx, "Name:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, item->name, NAME_SIZE - 1, nk_filter_default);
    
  nk_label(ctx, "Price:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, item->price_str, PRICE_SIZE - 1, nk_filter_float);

  nk_label(ctx, "Tag:", NK_TEXT_LEFT);
  item->tag = nk_combo(ctx, file_tags->tag_ptrs, file_tags->tags_count, item->tag, 25, nk_vec2(380,210));

  nk_label(ctx, "", NK_TEXT_LEFT);
  saveExpense(ctx, item, file_tags, expense_buffers, total_buffers);
}

void chooseDate(struct nk_context *ctx, char *buffer){
  static int date_selected = 0;
  static struct tm sel_date;
  if (date_selected == 0){
    time_t cur_time = time(0);
    struct tm *n = localtime(&cur_time);
    if (date_selected == 0) 
    memcpy(&sel_date, n, sizeof(struct tm));
  }
  sprintf(buffer, "%02d/%02d/%02d", sel_date.tm_mday, sel_date.tm_mon + 1, sel_date.tm_year + 1900);
  if (nk_combo_begin_label(ctx, buffer, nk_vec2(420,400))){
    static const char *month[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September","October", "November", "December"};
    static const char *week_days[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    static const int month_days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int i = 0;
    int year = sel_date.tm_year + 1900;
    int leap_year = (!(year % 4) && ((year % 100))) || !(year % 400);
    int days = (sel_date.tm_mon == 1) ? month_days[sel_date.tm_mon] + leap_year : month_days[sel_date.tm_mon];
    date_selected = 1;
    nk_layout_row_begin(ctx, NK_DYNAMIC, 25, 3);
    nk_layout_row_push(ctx, 0.06f);
    if (nk_button_symbol(ctx, NK_SYMBOL_TRIANGLE_LEFT)){
      if (sel_date.tm_mon == 0) {
        sel_date.tm_mon = 11;
        sel_date.tm_year = (0 > sel_date.tm_year - 1) ? 0 : sel_date.tm_year - 1;
      } else sel_date.tm_mon--;
    }
    nk_layout_row_push(ctx, 0.88f);
    sprintf(buffer, "%s %d", month[sel_date.tm_mon], year);
    nk_label(ctx, buffer, NK_TEXT_CENTERED);
    nk_layout_row_push(ctx, 0.06f);
    if (nk_button_symbol(ctx, NK_SYMBOL_TRIANGLE_RIGHT)){
      if (sel_date.tm_mon == 11) {
        sel_date.tm_mon = 0;
        sel_date.tm_year++;
      } else sel_date.tm_mon++;
    }
    nk_layout_row_end(ctx);
    int year_n = (sel_date.tm_mon < 2) ? year - 1 : year;
    int y = year_n % 100;
    int c = year_n / 100;
    int y4 = (int)((float)y / 4);
    int c4 = (int)((float)c / 4);
    int m = (int)(2.6 * (double)(((sel_date.tm_mon + 10) % 12) + 1) - 0.2);
    int week_day = ((1 + m + y + y4 + c4 - 2 * c) % 7);
    nk_layout_row_dynamic(ctx, 35, 7);
    for (i = 0; i < NUMBER_OF_WEEKDAYS; i++) 
      nk_label(ctx, week_days[i], NK_TEXT_CENTERED);
    if (week_day > 0) 
      nk_spacing(ctx, week_day);
    for (i = 1; i <= days; i++) {
      sprintf(buffer, "%d", i);
      if (nk_button_label(ctx, buffer)){
        sel_date.tm_mday = i;
        nk_combo_close(ctx);
      }
    }
    nk_combo_end(ctx);
  }
}

void saveExpense(struct nk_context *ctx, struct Item *item, struct FileTags *file_tags, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers){
  if (nk_button_label(ctx, "Save Expense")){
    item->result_code = checkInputBuffers(item);
    if (item->result_code == INPUT_OK){ 
      formatDateToISO(item->date);
      item->price = atof(item->price_str);
      addExpense(item->date, item->name, item->price, file_tags->tag_ptrs[item->tag]);
      clearItemTextboxes(item);
      reloadExpensesAndTotals(expense_buffers, total_buffers);
    } else clearItemTextboxes(item);
  }
}

void getData(struct nk_context *ctx, void (*clearFunction) (struct Item *item), struct Item *item){
  if(nk_button_label(ctx, "get data")){
    item->id = atoi(item->id_str); 
    item->get_data_pressed = 1;
    if (getFields(item->date, item->name, &item->price, item->tag_str, item->id)){ 
      unformatDateFromISO(item->date);
      sprintf(item->price_str, "%.13g", item->price); 
      item->id_found = 1;
    } else {
      item->id_found = 0;
      clearFunction(item);
    }
  }
}

void updateExpensesWindow(struct nk_context *ctx, struct Item *item, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers){
  errorLabels(ctx, item, "updated");
  
  static const float widget_sizes_first_row[] = {0.15, 0.50, 0.20};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 3, widget_sizes_first_row);
  
  nk_label(ctx, "ID:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, item->id_str, ID_SIZE - 1, nk_filter_decimal);
  getData(ctx, clearUpdateTextboxes, item);
    
  static const float widget_sizes[] = {0.15, 0.70};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 2, widget_sizes);

  nk_label(ctx, "Date:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, item->date, DATE_SIZE - 1, nk_filter_default);
  
  nk_label(ctx, "Name:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, item->name, NAME_SIZE - 1, nk_filter_default);
    
  nk_label(ctx, "Price:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, item->price_str, PRICE_SIZE - 1, nk_filter_float);

  nk_label(ctx, "Tag:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, item->tag_str, TAG_SIZE - 1, nk_filter_default);
  
  nk_label(ctx, "", NK_TEXT_LEFT);
  updateExpense(ctx, item, expense_buffers, total_buffers);
}

void updateExpense(struct nk_context *ctx, struct Item *item, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers){
  if (nk_button_label(ctx, "Update Expense")){
    item->result_code = checkUpdateBuffers(item);
    if (item->result_code == INPUT_OK){ 
      if (item->get_data_pressed == 1){ 
        if (item->id_found == 1){ 
          formatDateToISO(item->date);
          item->price = atof(item->price_str);
          amendExpense(item->date, item->name, item->price, item->tag_str, item->id);
          reloadExpensesAndTotals(expense_buffers, total_buffers);
          clearIdTextbox(item);
          clearUpdateTextboxes(item);
        } else item->result_code = NOT_EXIST;
        item->get_data_pressed = 0;  
      } else item->result_code = GET_DATA_NOT_PRESSED;
    } else clearUpdateTextboxes(item);
  }
}

void deleteExpensesWindow(struct nk_context *ctx, struct Item *item, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers){
  deleteAllExpenses(ctx, expense_buffers, total_buffers);
  errorLabels(ctx, item, "deleted");

  static const float widget_sizes_first_row[] = {0.15, 0.50, 0.20};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 3, widget_sizes_first_row);
  
  nk_label(ctx, "ID:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, item->id_str, ID_SIZE - 1, nk_filter_decimal);
  getData(ctx, clearDeleteLabels, item);
    
  static const float widget_sizes[] = {0.15, 0.70};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 2, widget_sizes);

  nk_label(ctx, "Date:", NK_TEXT_LEFT);
  nk_labelf(ctx, NK_TEXT_LEFT, "%s", item->date);
  
  nk_label(ctx, "Name:", NK_TEXT_LEFT);
  nk_labelf(ctx, NK_TEXT_LEFT, "%s", item->name);
  
  nk_label(ctx, "Price:", NK_TEXT_LEFT);
  nk_labelf(ctx, NK_TEXT_LEFT, "%s", item->price_str);
  
  nk_label(ctx, "Tag:", NK_TEXT_LEFT);
  nk_labelf(ctx, NK_TEXT_LEFT, "%s", item->tag_str);

  nk_label(ctx, "", NK_TEXT_LEFT);
  deleteExpense(ctx, item, expense_buffers, total_buffers);
}

void deleteAllExpenses(struct nk_context *ctx, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers){
  static int show_menubar = 0;  
  static int show_confirm_label = 0;
  struct nk_rect window_bounds = nk_window_get_bounds(ctx);
  if (nk_contextual_begin(ctx, 0, nk_vec2(220, 50), window_bounds)){
    nk_layout_row_dynamic(ctx, 30, 1);
    nk_selectable_label(ctx, show_menubar?"Hide Danger zone":"Show Danger zone", NK_TEXT_LEFT, &show_menubar);
    nk_contextual_end(ctx);
  }
  if (show_menubar){
    nk_menubar_begin(ctx);
    nk_layout_row_static(ctx, 30, 250, 1);
    if (nk_button_label(ctx, "Delete All Expenses!"))
      show_confirm_label = 1;
    if (show_confirm_label){
      static const float widget_sizes[] = {280, 150, 150};
      nk_layout_row(ctx, NK_STATIC, 40, 3, widget_sizes);
      nk_label(ctx, "Are you absolutely sure?", NK_TEXT_LEFT);
      if (nk_button_label(ctx, "Yes, I am")){
        eraseAllExpenses();
        reloadExpensesAndTotals(expense_buffers, total_buffers);
        show_confirm_label = 0;
      }
      if (nk_button_label(ctx, "No, Cancel")) 
        show_confirm_label = 0;
    }
    nk_menubar_end(ctx);
  }
}

void deleteExpense(struct nk_context *ctx, struct Item *item, struct ExpenseBuffers *expense_buffers, struct TotalBuffers *total_buffers){
  if (nk_button_label(ctx, "Delete Expense")){
    if (item->get_data_pressed == 1){ 
      if (item->id_found == 1){ 
        eraseExpense(item->id);
        reloadExpensesAndTotals(expense_buffers, total_buffers);
        clearIdTextbox(item);
        clearDeleteLabels(item);
      } else item->result_code = NOT_EXIST;
      item->get_data_pressed = 0;  
    } else item->result_code = GET_DATA_NOT_PRESSED;
  }
}

void totalsWindow(struct nk_context *ctx, struct TotalBuffers *total_buffers){
  static int show_menubar = 1;
  struct nk_rect window_bounds = nk_window_get_bounds(ctx);
  if (nk_contextual_begin(ctx, 0, nk_vec2(135, 50), window_bounds)){
    nk_layout_row_dynamic(ctx, 30, 1);
    nk_selectable_label(ctx, show_menubar?"Hide Filter":"Show Filter", NK_TEXT_LEFT, &show_menubar);
    nk_contextual_end(ctx);
  }
  if (show_menubar){
    nk_menubar_begin(ctx);
    dateFilterWidgetsInTotals(ctx, total_buffers);
    nk_menubar_end(ctx);
  }  
  
  static const float widget_sizes_table[] = {0.40, 0.30, 0.15};
  nk_layout_row(ctx, NK_DYNAMIC, 40, 3, widget_sizes_table);
  nk_label(ctx, "TAG", NK_TEXT_LEFT);
  nk_label(ctx, "TOTAL", NK_TEXT_CENTERED);
  nk_label(ctx, "COUNT", NK_TEXT_CENTERED);
  for (int i = 0; i < total_buffers->used_tags_count; i++){ 
    nk_labelf(ctx, NK_TEXT_LEFT, "%s", total_buffers->grouped_tag_ptrs[i]);
    nk_labelf(ctx, NK_TEXT_CENTERED, "%.13g", total_buffers->grouped_sums[i]);
    nk_labelf(ctx, NK_TEXT_CENTERED, "%d", total_buffers->grouped_counts[i]);
  }  
  nk_label(ctx, "Grand Total", NK_TEXT_LEFT);
  nk_labelf(ctx, NK_TEXT_CENTERED, "%.13g", total_buffers->total_sum);
  nk_labelf(ctx, NK_TEXT_CENTERED, "%d", total_buffers->expenses_count);
}

void dateFilterWidgetsInTotals(struct nk_context *ctx, struct TotalBuffers *total_buffers){
  static char start_date[DATE_SIZE], end_date[DATE_SIZE];
  static const float widget_sizes[] = {0.20, 0.25, 0.05, 0.25, 0.15, 0.05};
  nk_layout_row(ctx, NK_DYNAMIC, 35, 6, widget_sizes);
  nk_label(ctx, "Date Filter:", NK_TEXT_LEFT);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, start_date, DATE_SIZE - 1, nk_filter_default);
  nk_label(ctx, "to", NK_TEXT_CENTERED);
  nk_edit_string_zero_terminated(ctx, NK_EDIT_SIMPLE, end_date, DATE_SIZE - 1, nk_filter_default);  
  if (nk_button_label(ctx, "Apply")) applyDateFilterToTotals(total_buffers, start_date, end_date);
  if (nk_button_symbol(ctx, NK_SYMBOL_X)) clearDateFilterFromTotals(total_buffers, start_date, end_date);
}

void showAbout(struct nk_context *ctx){
  static int show_menubar = 0;
  struct nk_rect window_bounds = nk_window_get_bounds(ctx);
  if (nk_contextual_begin(ctx, 0, nk_vec2(150, 50), window_bounds)){
    nk_layout_row_dynamic(ctx, 30, 1);
    nk_selectable_label(ctx, show_menubar?"Hide About":"Show About", NK_TEXT_LEFT, &show_menubar);
    nk_contextual_end(ctx);
  }
  if (show_menubar){
    nk_menubar_begin(ctx);
    static const float about_widget_size[] = {80};
    nk_layout_row(ctx, NK_STATIC, 40, 1, about_widget_size);
    if (nk_widget_is_hovered(ctx))
      nk_tooltip(ctx, "Find out more at: https://codeberg.org/limisi/pet");
    nk_label(ctx, "ABOUT", NK_TEXT_LEFT);
    nk_menubar_end(ctx);
  }
}

void changeTheme(struct nk_context *ctx, struct Conf *conf){
  static int chosen_theme = 0;
  static const char *themes[] = {"RED", "WHITE", "BLUE", "BLACK", "DARK", "DRACULA"};
  static const float widget_sizes_menu[] = {180, 150, 100};
  nk_layout_row(ctx, NK_STATIC, 32, 3, widget_sizes_menu);
  nk_label(ctx, "Choose Theme: ", NK_TEXT_LEFT);
  chosen_theme = nk_combo(ctx, themes, 6, chosen_theme, 25, nk_vec2(130, 200));
  if (nk_button_label(ctx, "Apply")) {
    setStyle(ctx, chosen_theme);
    conf->theme = chosen_theme;
  } 
}

void settingsWindow(struct nk_context *ctx, struct UIState *ui_state, struct Conf *conf){
  showAbout(ctx);
  changeTheme(ctx, conf);
  
  static const float widget_sizes_password[] = {220, 215};
  nk_layout_row(ctx, NK_STATIC, 35, 2, widget_sizes_password);
  nk_label(ctx, "Change Password:", NK_TEXT_LEFT);
  if (nk_button_label(ctx, "Change Password")) proceedToChangePassword(ui_state);
  
  static const float widget_sizes_tags[] = {150, 285};
  nk_layout_row(ctx, NK_STATIC, 35, 2, widget_sizes_tags);
  nk_label(ctx, "Modify Tags:", NK_TEXT_LEFT);
  if (nk_button_label(ctx, "Modify Tags")) proceedToTags(ui_state);
}

void proceedToTags(struct UIState *ui_state){
  ui_state->show_expenses_ui = 0;
  ui_state->show_tags_ui = 1;
  ui_state->tags_loop_count = 0;
}

void proceedToChangePassword(struct UIState *ui_state){
  ui_state->show_expenses_ui = 0;
  ui_state->show_change_password_ui = 1;
}
