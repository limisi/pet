#ifndef itemSizes_h
#define itemSizes_h

#define ID_SIZE 10
#define DATE_SIZE 12
#define NAME_SIZE 31
#define PRICE_SIZE 15
#define TAG_SIZE 30

#endif
