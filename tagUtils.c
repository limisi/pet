#include <stdlib.h>//malloc in loadTagsFromFile
#include <string.h>//memset in clearNewTagsBoxes and clearTagBuffer
#include "lib/mpack.h"// file flushing and loading
#include "itemSizes.h"// TAG_SIZE
#include "tagUtils.h"
#include "paths.h"

void loadTagsFromFile(struct FileTags *file_tags){ 
  mpack_tree_t dummy_tree;
  mpack_tree_t *tree = &dummy_tree;
  mpack_tree_init_filename(tree, TAGS_FILE_PATH, 0);
  mpack_tree_parse(tree);
  mpack_node_t root = mpack_tree_root(tree);
  file_tags->tags_count = mpack_node_array_length(root);
  file_tags->tag_ptrs = malloc(sizeof(char**) * file_tags->tags_count);
  file_tags->editable_tags = malloc(file_tags->tags_count * TAG_SIZE);
  int buffer_len = 0;
  for (int i = 0; i < file_tags->tags_count; i++){
    buffer_len += mpack_node_strlen(mpack_node_array_at(root, i));
  }
  file_tags->tags_data = malloc(buffer_len + file_tags->tags_count);
  int tag_length = 0;
  int fixed_tag_length = 0;
  for (int i = 0; i < file_tags->tags_count; i++){
    mpack_node_copy_utf8_cstr(mpack_node_array_at(root, i), file_tags->tags_data + tag_length, buffer_len + file_tags->tags_count - tag_length);
    file_tags->tag_ptrs[i] = file_tags->tags_data + tag_length;
    strcpy(file_tags->editable_tags + fixed_tag_length, file_tags->tag_ptrs[i]);
    tag_length += mpack_node_strlen(mpack_node_array_at(root, i)) + 1;
    fixed_tag_length += TAG_SIZE;
  }
  if (mpack_tree_destroy(tree) != mpack_ok) 
    printf("Mpack: tree error in loadTagsFromFile\n");
}

void freeTags(struct FileTags *file_tags){
  free(file_tags->tags_data);
  free(file_tags->tag_ptrs);
  free(file_tags->editable_tags);
}

void reloadTags(struct FileTags *file_tags){
  freeTags(file_tags);
  loadTagsFromFile(file_tags);
}

void saveNewTags(struct FileTags *file_tags, char new_tags[NUMBER_OF_NEW_TAGS_BOXES][TAG_SIZE]){
  mpack_writer_t dummy_writer;
  mpack_writer_t *writer = &dummy_writer;
  mpack_writer_init_filename(writer, TAGS_FILE_PATH);
  mpack_build_array(writer);
  for (int i = 0; i < file_tags->tags_count; i++)
    mpack_write_utf8_cstr(writer, file_tags->tag_ptrs[i]);
  for (int i = 0; i < NUMBER_OF_NEW_TAGS_BOXES; i++)
    if (new_tags[i][0] != '\0') mpack_write_utf8_cstr(writer, new_tags[i]);
  mpack_complete_array(writer);
  if (mpack_writer_destroy(writer) != mpack_ok) 
    printf("Mpack: writer error in saveNewTags\n"); 
  reloadTags(file_tags);
  clearNewTagsBoxes(new_tags);
}

void clearNewTagsBoxes(char new_tags[NUMBER_OF_NEW_TAGS_BOXES][TAG_SIZE]){
  for(int i = 0; i < NUMBER_OF_NEW_TAGS_BOXES; i++) memset(new_tags[i], '\0', TAG_SIZE);
}

void editTags(struct FileTags *file_tags){
  int tag_length = 0;
  mpack_writer_t dummy_writer;
  mpack_writer_t *writer = &dummy_writer;
  mpack_writer_init_filename(writer, TAGS_FILE_PATH);
  mpack_build_array(writer);
  for (int i = 0; i < file_tags->tags_count; i++){
    if (strlen(file_tags->editable_tags + tag_length) != 0) 
      mpack_write_utf8_cstr(writer, file_tags->editable_tags + tag_length);
    tag_length += TAG_SIZE;
  }
  mpack_complete_array(writer);
  if (mpack_writer_destroy(writer) != mpack_ok) 
    printf("Mpack: writer error in editTags\n");
  reloadTags(file_tags);
}

void clearTagBuffer(char *tag_buffer){
  memset(tag_buffer, '\0', strlen(tag_buffer));
}
