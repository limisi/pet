#ifndef passwordUI_h
#define passwordUI_h

#include "conf.h"
#include "UIState.h"

void nameErrorLabels(struct nk_context *ctx, int *name_result_code);
void passwordErrorLabels(struct nk_context *ctx, int *password_result_code);
void setPasswordWindow(struct nk_context *ctx, struct Conf *conf, struct UIState *ui_state);
void proceedToLogin(struct UIState *ui_state);
void enterPasswordWindow(struct nk_context *ctx, struct Conf *conf, struct UIState *ui_state);
void proceedToExpensesFromLogin(struct UIState *ui_state);
void changePasswordWindow(struct nk_context *ctx, struct Conf *conf, struct UIState *ui_state);
void returnToExpenses(struct UIState *ui_state);

#endif
