#ifndef UIState_h
#define UIState_h

struct UIState{
  int show_expenses_ui;
  int show_tags_ui;
  int show_set_password_ui;
  int show_enter_password_ui;
  int show_change_password_ui;
  int expenses_loop_count;
  int tags_loop_count;
};

#endif
