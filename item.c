#include "item.h"
#include "dates.h"//isDateValid, isDateFormatValid and padDate in checkUpdateBuffers
#include <string.h>//strlen in check functions, memset in the clear buffer functions, strcpy in clearDeleteLabels

int checkInputBuffers(struct Item *item){
  if (strlen(item->name) == 0 || strlen(item->price_str) == 0) return BLANK_FIELD;
  else return INPUT_OK;
}

int checkUpdateBuffers(struct Item *item){
  if (strlen(item->date) == 0 || strlen(item->name) == 0 || strlen(item->price_str) == 0 || strlen(item->tag_str) == 0)
    return BLANK_FIELD;
  
  padDate(item->date);
  if (!isDateFormatValid(item->date)) 
    return INVALID_DATE_FMT;
  if (!isDateValid(item->date))
    return INVALID_DATE;
  
  return INPUT_OK;
}

void clearItemTextboxes(struct Item *item){
  memset(item->name, '\0', strlen(item->name));
  memset(item->price_str, '\0', strlen(item->price_str));
}

void clearIdTextbox(struct Item *item){
  memset(item->id_str, '\0', strlen(item->id_str));
}

void clearUpdateTextboxes(struct Item *item){
  memset(item->date, '\0', strlen(item->date));
  memset(item->name, '\0', strlen(item->name));
  memset(item->price_str, '\0', strlen(item->price_str));
  memset(item->tag_str, '\0', strlen(item->tag_str));
}

void clearDeleteLabels(struct Item *item){
  strcpy(item->date, "---");
  strcpy(item->name, "---");
  strcpy(item->price_str, "---");
  strcpy(item->tag_str, "---");
}


