#include <stdlib.h>
/*
calloc - (4 calls) to allocate memory for item_input, item_update, item_delete, ui_state where zero initialization is handy
malloc - (4 calls) to allocate memory for conf, file_tags, expense_buffers, total_buffers 
free - (8 calls) free memory allocated to item_input, item_update, item_delete, ui_state, conf, file_tags, expense_buffers, total_buffers 
*/

#include <stdbool.h> // true - used in glfwSetWindowShouldClose

#include "lib/gl.h"
/*
gladLoadGL - loads OpenGL functions
glClear - clears the screen at the end of each frame
glClearColor - sets the default colour to clear the screen with
glViewport - sets drawing bounds for OpenGL
*/

#include <GLFW/glfw3.h>
/*
glfwWindowShouldClose - sets window to close when <esc> is pressed
glfwSetErrorCallback - sets error callback to errorCallback which prints the GLFW error code and description
glfwInit
glfwWindowHint - (4 calls) used to set OpenGL version to 3.3, set OpenGL to core profile, enable forward compatability for MacOS
glfwCreateWindow
glfwMakeContextCurrent - sets window as primary OpenGL context i.e where OpenGL will draw UI to
glfwSetFramebufferSizeCallback - sets the framebuffer size callback to windowSizeCallback which resizes the OpenGL viewport whenever the window is resized
glfwGetWindowSize - obtain window width and height which are used to resize OpenGL viewport and Nuklear windows 
glfwSetWindowIcon
glfwGetKey - to check whether <esc> is pressed, so as to trigger glfwSetWindowShouldClose
glfwSetWindowShouldClose - terminates render loop hence closing pet
glfwPollEvents
glfwTerminate - frees memory used and allocated by GLFW
glfwWaitEvents
glfwSwapBuffers
*/

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_KEYSTATE_BASED_INPUT
#include "lib/nuklear.h"
/*
nk_begin - (12 calls) create Nuklear windows
nk_rect - (12 calls) set dimensions and positions of Nuklear windows
nk_end - (12 calls) clear Nuklear windows after each frame
nk_font_atlas_add_from_file - load NotoSans-Regular font
nk_style_set_font - set font to NotoSans-Regular
*/

#include "lib/nuklear_glfw_gl3.h"
/*
nk_glfw3_new_frame
nk_glfw3_render
nk_glfw3_shutdown
nk_glfw3_init
nk_glfw3_font_stash_begin
nk_glfw3_font_stash_end
*/

#define QOI_IMPLEMENTATION
#include "lib/qoi.h"//qoi_read, qoi_desc in loadIcons - TODO

#include "lib/style.h"//setStyle - sets the theme

#include "dbs.h"
/*
initDB - opens accounts.db SQLite3 database
createTable - creates the expenses table
closeDb
*/

#include "load.h"// setStats, loadExpenses, freeExpenseBuffers, loadTotals, freeTotalBuffers
#include "tagUtils.h"// loadTagsFromFile
#include "item.h"// clearDeleteLabels

#include "UIState.h"// UIState struct
#include "expensesUI.h"// viewExpensesWindow, inputExpensesWindow, updateExpensesWindow, deleteExpensesWindow, settingsWindow, collapseExpensesWindowsOnFirstDraw
#include "passwordUI.h"// setPasswordWindow, enterPasswordWindow
#include "tagsUI.h"// addTagsWindow, editTagsWindow, backToExpensesWindow, collapseTagsWindowsOnFirstDraw
#include "conf.h"
#include "paths.h"

#define WINDOW_WIDTH 700
#define WINDOW_HEIGHT 600

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024

#define WINDOW_BAR_OFFSET 49

void windowSizeCallback(GLFWwindow *window, int width, int height);
void errorCallback(int error, const char *description);
void initWindow(GLFWwindow **window, int *width, int *height);
void loadIcons(GLFWwindow *window);
void initNk(GLFWwindow *window, struct nk_glfw *glfw, struct nk_context **ctx);
void processInput(GLFWwindow *window);

int main(void){
  GLFWwindow *window;
  struct nk_context *ctx;
  struct nk_glfw glfw = {0};
  int width = 0, height = 0;

  struct Conf *conf = malloc(sizeof(struct Conf));
  loadConfigurations(conf);

  struct FileTags *file_tags = malloc(sizeof(struct FileTags));
  loadTagsFromFile(file_tags);
  
  initDb();

  createTable();
  
  struct Item *item_input = calloc(1, sizeof(struct Item));
  item_input->result_code = INPUT_OK;

  struct Item *item_update = calloc(1, sizeof(struct Item));
  item_update->result_code = INPUT_OK;

  struct Item *item_delete = calloc(1, sizeof(struct Item));
  item_delete->result_code = INPUT_OK;
  clearDeleteLabels(item_delete);

  struct ExpenseBuffers *expense_buffers = malloc(sizeof(struct ExpenseBuffers));
  expense_buffers->chosen_field = 0;
  expense_buffers->chosen_order = 0;
  expense_buffers->filter_name = "'%'";
  expense_buffers->start_date = "(SELECT MIN(date) FROM expenses)";
  expense_buffers->end_date = "(SELECT MAX(date) FROM expenses)";
  loadExpenses(expense_buffers);
  
  struct TotalBuffers *total_buffers = malloc(sizeof(struct TotalBuffers));
  total_buffers->start_date = "(SELECT MIN(date) FROM expenses)";
  total_buffers->end_date = "(SELECT MAX(date) FROM expenses)";
  setStats(total_buffers);
  loadTotals(total_buffers);

  struct UIState *ui_state = calloc(1, sizeof(struct UIState));
  if (conf->password_set == 0) ui_state->show_set_password_ui = 1;
  else ui_state->show_enter_password_ui = 1;

  initWindow(&window, &width, &height);
  loadIcons(window);
  
  initNk(window, &glfw, &ctx); 
  setStyle(ctx, conf->theme);
  
  while (!glfwWindowShouldClose(window)) {
    processInput(window);
    nk_glfw3_new_frame(&glfw);
    glfwGetWindowSize(window, &width, &height);

    if (ui_state->show_expenses_ui == 1){ 
      if (nk_begin(ctx, "View Expenses", nk_rect(0, 0, width, height), NK_WINDOW_MINIMIZABLE)) 
        viewExpensesWindow(ctx, expense_buffers);
      nk_end(ctx);
      if (nk_begin(ctx, "Input Expenses", nk_rect(0, WINDOW_BAR_OFFSET, width, height - WINDOW_BAR_OFFSET), NK_WINDOW_MINIMIZABLE))  
        inputExpensesWindow(ctx, item_input, file_tags, expense_buffers, total_buffers);
      nk_end(ctx);
      if (nk_begin(ctx, "Update Expenses", nk_rect(0, WINDOW_BAR_OFFSET * 2, width, height - WINDOW_BAR_OFFSET * 2), NK_WINDOW_MINIMIZABLE))  
        updateExpensesWindow(ctx, item_update, expense_buffers, total_buffers);
      nk_end(ctx);
      if (nk_begin(ctx, "Delete Expenses", nk_rect(0, WINDOW_BAR_OFFSET * 3, width, height - WINDOW_BAR_OFFSET * 3), NK_WINDOW_MINIMIZABLE))  
        deleteExpensesWindow(ctx, item_delete, expense_buffers, total_buffers);
      nk_end(ctx);
      if (nk_begin(ctx, "Totals", nk_rect(0, WINDOW_BAR_OFFSET * 4, width, height - WINDOW_BAR_OFFSET * 4), NK_WINDOW_MINIMIZABLE))
        totalsWindow(ctx, total_buffers);
      nk_end(ctx);
      if (nk_begin(ctx, "Settings", nk_rect(0, WINDOW_BAR_OFFSET * 5, width, height - WINDOW_BAR_OFFSET * 5), NK_WINDOW_MINIMIZABLE))
        settingsWindow(ctx, ui_state, conf);
      nk_end(ctx);

      collapseExpensesWindowsOnFirstDraw(ctx, ui_state);
    }
    
    if (ui_state->show_tags_ui == 1){ 
      if (nk_begin(ctx, "Add Tags", nk_rect(0, 0, width, height), NK_WINDOW_MINIMIZABLE))   
        addTagsWindow(ctx, file_tags, ui_state);
      nk_end(ctx);
      if (nk_begin(ctx, "Edit Tags", nk_rect(0, WINDOW_BAR_OFFSET, width, height - WINDOW_BAR_OFFSET), NK_WINDOW_MINIMIZABLE)) 
        editTagsWindow(ctx, file_tags, ui_state);
      nk_end(ctx);
     if (nk_begin(ctx, "Back To Expenses", nk_rect(0, WINDOW_BAR_OFFSET * 2, width, height - WINDOW_BAR_OFFSET * 2), NK_WINDOW_BORDER)) 
        backToExpensesWindow(ctx, ui_state);
      nk_end(ctx);
      collapseTagsWindowsOnFirstDraw(ctx, ui_state);
    }

    if (ui_state->show_set_password_ui == 1){
      if (nk_begin(ctx, "Set Password", nk_rect(0, 0, width, height), NK_WINDOW_BORDER)) 
        setPasswordWindow(ctx, conf, ui_state);
      nk_end(ctx);
    }

    if (ui_state->show_enter_password_ui == 1){
      if (nk_begin(ctx, "Enter Password", nk_rect(0, 0, width, height), NK_WINDOW_BORDER)) 
        enterPasswordWindow(ctx, conf, ui_state);
      nk_end(ctx);
    }
    
    if (ui_state->show_change_password_ui == 1){
      if (nk_begin(ctx, "Change Password", nk_rect(0, 0, width, height), NK_WINDOW_BORDER)) 
        changePasswordWindow(ctx, conf, ui_state);
      nk_end(ctx);
    }
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(0.1f, 0.18f, 0.24f, 1.0f);
    nk_glfw3_render(&glfw, NK_ANTI_ALIASING_ON, MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);
    glfwSwapBuffers(window);
    glfwWaitEvents();
  }
  nk_glfw3_shutdown(&glfw);
  glfwTerminate(); 

  closeDb();

  saveConfigurations(conf);
  
  free(item_input);
  free(item_update);
  free(item_delete);

  freeTags(file_tags);
  free(file_tags);
  
  freeExpenseBuffers(expense_buffers);
  free(expense_buffers);
  
  freeTotalBuffers(total_buffers);
  free(total_buffers);

  free(conf);
  free(ui_state);
  
  return 0;
}

void windowSizeCallback(GLFWwindow *window, int width, int height){
  glViewport(0, 0, width, height);
}

void errorCallback(int error, const char *description){
  printf("Error %d: %s\n", error, description);
}

void initWindow(GLFWwindow **window, int *width, int *height){ 
  glfwSetErrorCallback(errorCallback);
  if (!glfwInit()) {
    printf("GLFW: failed to init!\n");
    exit(EXIT_FAILURE);
  }
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
  *window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "pet", NULL, NULL);
  glfwMakeContextCurrent(*window);
  glfwSetFramebufferSizeCallback(*window, windowSizeCallback);
  glfwGetWindowSize(*window, width, height);
  if(!gladLoadGL((GLADloadfunc)glfwGetProcAddress)){
    printf("OpenGL: failed to init!\n");
    exit(EXIT_FAILURE);
  }
}

void loadIcons(GLFWwindow *window){
  qoi_desc icon16_desc;
  qoi_desc icon32_desc;
  qoi_desc icon48_desc;
  qoi_desc icon64_desc;
  qoi_desc icon128_desc;
  qoi_desc icon256_desc;
  
  GLFWimage icons[6];

  icons[0].pixels = qoi_read(PETICON16_PATH, &icon16_desc, 0);
  icons[0].width = icon16_desc.width;
  icons[0].height = icon16_desc.height;
  
  icons[1].pixels = qoi_read(PETICON32_PATH, &icon32_desc, 0);
  icons[1].width = icon32_desc.width;
  icons[1].height = icon32_desc.height;
  
  icons[2].pixels = qoi_read(PETICON48_PATH, &icon48_desc, 0);
  icons[2].width = icon48_desc.width;
  icons[2].height = icon48_desc.height;

  icons[3].pixels = qoi_read(PETICON64_PATH, &icon64_desc, 0);
  icons[3].width = icon64_desc.width;
  icons[3].height = icon64_desc.height;

  icons[4].pixels = qoi_read(PETICON128_PATH, &icon128_desc, 0);
  icons[4].width = icon128_desc.width;
  icons[4].height = icon128_desc.height;

  icons[5].pixels = qoi_read(PETICON256_PATH, &icon256_desc, 0);
  icons[5].width = icon256_desc.width;
  icons[5].height = icon256_desc.height;

  glfwSetWindowIcon(window, 6, icons);

  free(icons[0].pixels);
  free(icons[1].pixels);
  free(icons[2].pixels);
  free(icons[3].pixels);
  free(icons[4].pixels);
  free(icons[5].pixels); 
}

void initNk(GLFWwindow *window, struct nk_glfw *glfw, struct nk_context **ctx){
  *ctx = nk_glfw3_init(glfw, window, NK_GLFW3_INSTALL_CALLBACKS);
  struct nk_font_atlas *atlas;
  nk_glfw3_font_stash_begin(glfw, &atlas);
  struct nk_font *noto = nk_font_atlas_add_from_file(atlas, FONT_FILE_PATH, 32, 0);
  nk_glfw3_font_stash_end(glfw);
  nk_style_set_font(*ctx, &noto->handle);
}

void processInput(GLFWwindow *window){
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) 
    glfwSetWindowShouldClose(window, true);
  glfwPollEvents();
}
