#ifndef conf_h
#define conf_h

#include "passwordConstants.h"

struct Conf{
  int theme;
  int password_set;
  int encoded_length;
  char name[USERNAME_CAPACITY];
  char hash[HASH_CAPACITY];
  char salt[SALT_CAPACITY];
};

void loadConfigurations(struct Conf *conf);
void saveConfigurations(struct Conf *conf);

#endif
