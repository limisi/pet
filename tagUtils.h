#ifndef tagUtils_h
#define tagUtils_h

#include "itemSizes.h"//TAG_SIZE

#define NUMBER_OF_NEW_TAGS_BOXES 5

struct FileTags{
  char *tags_data;
  const char **tag_ptrs;
  int tags_count;
  char *editable_tags;
};

void loadTagsFromFile(struct FileTags *file_tags); 
void freeTags(struct FileTags *file_tags);
void reloadTags(struct FileTags *file_tags);

void saveNewTags(struct FileTags *file_tags, char new_tags[NUMBER_OF_NEW_TAGS_BOXES][TAG_SIZE]);
void clearNewTagsBoxes(char new_tags[NUMBER_OF_NEW_TAGS_BOXES][TAG_SIZE]);

void editTags(struct FileTags *file_tags);
void clearTagBuffer(char *tag_buffer);

#endif
