#ifndef passwordConstants_h
#define passwordConstants_h

#define USERNAME_CAPACITY 30
#define PASSWORD_CAPACITY 20

#define SALT_LENGTH 32
#define HASH_LENGTH 64

#define SALT_CAPACITY 40
#define HASH_CAPACITY 200

#define MINIMUM_PASSWORD_LENGTH 8
#define CHARSET_LENGTH 62

#endif
